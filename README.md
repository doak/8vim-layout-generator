## Generator for 8VIM's keyboard layouts


### What is it about?

This is an alternative generator for [8VIM][8vim] keyboard layout. The idea is based on the
[generator of _sslater11_][original-generator], though its more sophisticated and unfortunately much
more code (which I am not proud of).

[8vim]: https://github.com/flide/8VIM
[original-generator]: https://github.com/sslater11/8vim_keyboard_layout_file_generator

The main demand for me was to be able to try out different concepts of layouts more quickly. Hence I
wanted to be able to swap characters on the main keyboard, without the hassle to update tons of
"depending" strokes like diacritics. Furthermore it should be "easy" to define these "dependent"
strokes, i.e. strokes which are based on another stroke to modify its meaning. This can be used to
input a capitalised letter or, like mentioned, a diacritic.

The very first layout I have implemented is the mix of the German base layout
[de_regular_glitchy_tozier_draft_german.xml][layout:de-base] by _Glitchy-Tozier_ and the very
comprehensive Layout [de_modified_german_220209.xml.zip][layout:kjoetom-de] with tons of diacritics
and nice ideas by _kjoetom_.

[layout:de-base]: https://github.com/flide/8VIM/commit/f5087f7a9a802b753fec41f2214490b917df88a1?diff=unified#diff-de8aacb8c5a535be99d70e5068e6ff772a4ab0729602af2726a15fd269125855
[layout:kjoetom-de]: https://github.com/flide/8VIM/files/8028422/de_modified_german_220209.xml.zip


### Features / Future / Limitations

Main features:
* Create modified variants of existing strokes.

This supports to quickly create strokes for diacritics or capitals (shifted keys) as well as
combinations with other key modifier like Ctrl key, too. Additionally this supports rearranging the
base layout without adapting the "dependent" strokes.

(Not yet existing) Features of 8VIM I have in mind:
* Several x-pad layouts, i.e. arbitrary number of layers.
* X-pad layout with another (i.e. != 4) count of sectors.
* Some other fancy ideas I have, please refer to [that post][1] which describes the ideas which 
  came to my mind.

Limitations:
* I don't have any real clue about the XML schema used by 8VIM. I have just replicated what I
  have seen so far. In particular I did not investigate the `<flags>` tag and just don't use it.
* No support for modifiers like Ctrl etc. (this related most likely with the point above).

[1]: https://github.com/flide/8VIM/discussions/65#discussioncomment-188129


### Overview

The heart of this concept is the class `StrokeEditor` which can be used to develop strokes, usually
based on an existing one. The API for this is "fluent", e.g.
`existingStroke.insert().rwd().fwd(2).finish()`. This returns a new Stroke definition, which starts
like `existingStroke`, but, at the very last sector, goes back one sector, forwards again, and move
one additional sector forward.
The clue, to utilise this API from the layout configuration, is the class `LayerEditor`. This class
provides methods which you can directly "access" from the layout configuration and provides
(standard) modifications to strokes, like the well known "add one full circle to capitalise" thingy.
You are able to just extend this class to provide additional modifications concepts. Just hardcode
it there, inherit from it or its base class `LayerEditorBase` and provide methods like the mentioned
one:
```python
def fullCircleToSwapShifted(self, *inputs):
    for input in inputs:
        stroke = self.mappings[input][0]
        input, shiftedInput = self.mappings[stroke]
        newStroke = stroke.insert().fwd(-1).finish()
        self.mappings.add(shiftedInput, input, newStroke)
```
You get the context, that is the current base layer (derived from class `Layer`) and the current
`Mappings` instance, as `self.layer` and `self.mappings` respectively. Usually the latter is enough.
You may use it to get the existing (first) stroke for e.g the letter `a` and modify it. In this
example we enter insert mode (i.e. the to-be-added sectors are inserted just before the cursor
(which is set to the end by default)), and add a full circle keeping direction (i.e. forward ==
`fwd()`). The argument `-1` is special and means full circle.
The arguments to this functions are deduced directly form the "calling" line in the layout
configuration, e.g.:
```
LayerEditor.fullCircleToSwapShifted:   a b c d
```
The line is split at white spaces and all arguments are passed to the method. Hence in this case,
the method would be add modified strokes based on the strokes for `a`, `b`, `c` and `d`. The
existing mapping for the existing stroke is returned by the (second) call to the index operator
using that stroke. These two mappings, the first for unshifted/regular letter, the second for the
shifted letter, are then used for the new mapping, though swapped to achieve the shift toggle.

Other methods works a little bit different and use the very first argument to define the base
stroke, and add a mapping for every following argument, e.g.:
```
LayerEditor.rwdsAtFinalSector:          a   ä Ä á Á à À æ Æ å Å â Â ą Ą ã Ã ā Ā
```

There is one special method: `LayerEditor.hardcode()`. It allows you to craft a stroke by hand.

Currently the following methods are provided. Its just the stuff I came up with to implement the
very first layout:
* `fullCircleToSwapShifted(self, *inputs)`
* `rwdFwdBeforeLastCircleToSwapShift(self, *inputs)`
* `rwdFwdsAtFinalSector(self, basedOnStrokeFor, *inputs)`
* `rwdFwdFwdsAtFinalSector(self, basedOnStrokeFor, *inputs)`
* `rwdsAtFinalSector(self, basedOnStrokeFor, *inputs)`
* `prefixCtcAndExitNotouch(self, basedOnStrokeFor, exitSector, *inputs)`
* `hardcode(self, input, shiftedInput, *sectors)`
