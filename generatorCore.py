from copy import copy
from itertools import zip_longest, chain
from sys import stderr


class InvalidFormat(Exception):
    pass


class Layer:
    _CIRCLE = -1
    _NO_TOUCH = -2

    def __init__(self, name, sectorNames):
        self._name = name
        self._nSectors = len(sectorNames)
        self._sectorNames = tuple(sectorNames)

    def __eq__(self, rhs):
        if self._name != rhs._name:
            return False
        if self._nSectors != rhs._nSectors or self._sectorNames != rhs._sectorNames:
            raise RuntimeError("Name of layer is the same, but it differs fundamentally")
        return True

    def __str__(self):
        return self._name

    def getName(self):
        return self._name

    def getNSectors(self):
        return self._nSectors

    def encode(self, sectors):
        def translate(sector):
            if type(sector) == int:
                if sector == self._CIRCLE:
                    return "INSIDE_CIRCLE"
                if sector == self._NO_TOUCH:
                    return "NO_TOUCH"
                try:
                    return self._sectorNames[sector]
                except (ValueError, TypeError):
                    raise ValueError(f"Invalid sector value: '{sector}'")
            return sector
        return [ translate(s) for s in sectors ]

    def decode(self, sectors):
        def translate(sector):
            if type(sector) == str:
                if sector == "NO_TOUCH":
                    return self._NO_TOUCH
                if sector == "INSIDE_CIRCLE":
                    return self._CIRCLE
                try:
                    return self._sectorNames.index(sector)
                except ValueError:
                    raise ValueError(f"Invalid sector name: '{sector}'")
            return sector
        return [ translate(s) for s in sectors ]


class BasicLayer(Layer):
    def __init__(self, lowerChars, upperChars, name):
        self._lowerChars = lowerChars
        self._upperChars = upperChars
        self._sectorSpec = None
        self._extendRules = []

        nSectors = self._parseChars()
        sectorNames = self._initSectorNames(nSectors)
        super().__init__(name, sectorNames)

    def extend(self, extendRule, *args):
        self._extendRules += [[extendRule, args]]

    def getMappings(self):
        mappings = Mappings()
        for (startSector, direction), arm in self._sectorSpec.items():
            for steps, (input, shiftedInput) in enumerate(arm):
                try:
                    steps += 1
                    strokeEditor = Stroke(self, self._CIRCLE, startSector, self._CIRCLE).insert()
                    if direction == "cw":
                        strokeEditor.cw(steps)
                    elif direction == "ccw":
                        strokeEditor.ccw(steps)
                    else:
                        assert(False and f"Invalid direction: '{direction}'")
                    mappings.add(input, shiftedInput, strokeEditor.finish())
                except Exception as ex:
                    raise InvalidFormat(f"Failed to set stroke for '{(input, shiftedInput)}'") from ex

        layerEditor = LayerEditorBase(self, mappings)
        for extendRule, args in self._extendRules:
            try:
                extendRule(layerEditor, *args)
            except Exception as ex:
                raise InvalidFormat(f"Failed to edit stroke with '{extendRule.__name__}{args}'")

        return mappings

    def getXmlCharacterSet(self):
        return (
            self._convertIntoXmlCharacterSet(self._lowerChars),
            self._convertIntoXmlCharacterSet(self._upperChars)
        )

    def __str__(self):
        if self._sectorSpec == None:
            return "<noLoaded>"

        sectors = self._sectorSpec.items()
        return f"{super().__str__()}:\n" + "\n".join(
            (f"{start[0]}:{start[1]:3} {chars}" for start, chars in sorted(sectors))
        )

    def _parseChars(self):
        armsForLower = self._splitIntoArms(self._lowerChars)
        armsForUpper = self._splitIntoArms(self._upperChars)
        if (len(armsForLower) != len(armsForUpper)):
            raise InvalidFormat("Inconsistent number of sectors for upper and lower characters")
        if (len(armsForLower) % 2 != 0):
            raise InvalidFormat("Missing clockwise definition for sector")
        # First and second shall be cw and ccw of first sector, etc.
        armsForLower = armsForLower[-1:] + armsForLower[:-1]
        armsForUpper = armsForUpper[-1:] + armsForUpper[:-1]

        arms = [zip_longest(l, u) for l, u in zip(armsForLower, armsForUpper)]

        nSectors = len(arms) // 2

        sectorSpec = {
            (i, direction): list(arms[i*2 + (direction == "cw")])
            for direction in ("ccw", "cw")
            for i in range(nSectors)
        }
        self._sectorSpec = sectorSpec

        return nSectors

    def _initSectorNames(self, nSectors):
        # Only two sectors does not work (currently), because direction can't
        # be determind by sector order alone.
        if nSectors in (3, 4):
            availableNames = ["TOP", "RIGHT", "BOTTOM", "LEFT"]
        elif nSectors in (5, 6, 7, 8):
            availableNames = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
        else:
            raise InvalidFormat(f"Unsupported number of sectors : expected between 3 and 8, but got {nSectors}.")

        names = selectNOutOfSequence(availableNames, nSectors)
        return names

    @classmethod
    def _convertIntoXmlCharacterSet(cls, chars):
        # Convert from our nice layout string to the ugly one that 8VIM uses.
        arms = cls._splitIntoArms(chars)
        # Fill up to 4 charaters per arm.
        arms = [ a + " " * (4 - len(a)) for a in arms ]
        if len(arms)//2 != 4:
            raise NotImplementedError(f"A layout with {len(arms)//2} sectors are not supported currently")
        arms = arms[2:] + arms[:2]
        characterSet=""
        for cw, ccw in pairwise(arms):
            for t in zip_longest(cw, ccw):
                characterSet+= t[0] + t[1]
        return characterSet

    @classmethod
    def _splitIntoArms(cls, chars):
        # Space is not supported as character here, but is the separator.
        return [ arm for arm in chars.split(" ") if arm ]


class InvalidStroke(Exception):
    pass


class Stroke:
    def __init__(self, baseLayer, *sectors):
        self._baseLayer = baseLayer
        self._boundaries = (baseLayer._CIRCLE, baseLayer._NO_TOUCH)
        self._stroke = self._decode(*sectors)

    def __eq__(self, rhs):
        return self.getLayerName() == rhs.getLayerName() and self._stroke == rhs._stroke

    def __hash__(self):
        return hash((self.getLayerName(), tuple(self._stroke)))

    def __copy__(self):
       stroke = Stroke(self._baseLayer, *self._stroke)
       return stroke

    def __str__(self):
        return ";".join(self._encode())

    # Iterate over stroke sections.
    def __iter__(self):
        return ( Stroke(self._baseLayer, *section) for section in forEachPrefix(self._stroke, *self._boundaries) )

    def getLayerName(self):
        return self._baseLayer.getName()

    def insert(self, inPlace=False, **kwargs):
        return StrokeEditor(self, "insert", inPlace=inPlace, **kwargs)

    def append(self, inPlace=False, **kwargs):
        return StrokeEditor(self, "append", inPlace=inPlace, **kwargs)

    def check(self):
        self._checkBoundary()
        self._checkContinuous()

    def _checkBoundary(self):
        for i, msg in [(0, "start"), (-1, "end")]:
            sector = self._stroke[i]
            if sector not in self._boundaries:
                raise InvalidStroke(f"'{self._encode(sector)[0]}' is not a valid {msg} for stroke: {self}")

    def _checkContinuous(self):
        for i, sector in enumerate(self._stroke):
            if sector in self._boundaries:
                lastSector = None
                continue
            elif lastSector != None:
                if abs(sector - lastSector) not in (1, self._baseLayer.getNSectors() - 1):
                    lastSector, sector = self._encode(lastSector, sector)
                    raise InvalidStroke(f"Non-continues stroke '{self}' at index {i}: {lastSector} → {sector}")
            lastSector = sector

    def _modulo(self, sectors):
        n = self._baseLayer.getNSectors()
        return [ s % n for s in sectors ]

    def _encode(self, *sectors):
        if not sectors:
            sectors = self._stroke
        return self._baseLayer.encode(sectors)

    def _decode(self, *sectors):
        return self._baseLayer.decode(sectors)


class StrokeEditor:
    def __init__(self, stroke, editMode, inPlace=False, **kwargs):
        if not inPlace:
            stroke = copy(stroke)
        self._obj = stroke
        if editMode not in ("insert", "append"):
            raise ValueError(f"Unknown edit mode: '{editMode}'")
        self._editMode = editMode
        self._cursor = -1
        getattr(self, editMode)(**kwargs)

        # Only calculcate the direction once (as soon as needed) for the least suprise.
        self._direction = None

        self._marks = []

    # Set cursor to specified mark.
    def __getitem__(self, key):
        try:
            self._cursor = self._calcCursor(self._cursor, mark=key)
        except IndexError:
            raise ValueError(f"Mark '{key}' does not exist: {self}")
        return self

    def __str__(self):
        cursor = self._cursor
        stroke = self._obj._stroke
        enc = self._obj._encode
        symbol = "▮"
        if self._editMode == "append":
            if cursor == -1:
                cursor = len(stroke)
            else:
                cursor += 1
            symbol = "▬"

        stroke = stroke[:cursor] + [symbol] + stroke[cursor:]
        marks = ""
        if self._marks:
            marks = f" marks:{self._marks}"
        return ";".join(enc(*stroke)) + marks

    def insert(self, **kwargs):
        self._editMode = "insert"
        self._cursor = self._calcCursor(self._cursor, **kwargs)
        return self

    def append(self, **kwargs):
        self._editMode = "append"
        self._cursor = self._calcCursor(self._cursor, **kwargs)
        return self

    def search(self, *lookForSectors, reverse=False):
        if len(lookForSectors) == 0:
            lookForSectors = self._obj._boundaries
        lookForSectors = self._obj._decode(*lookForSectors)

        selector = lambda x: x[1] in lookForSectors
        cursor = self._cursor
        sectors = enumerate(self._obj._stroke)
        if reverse:
            sectors = list(sectors)[:cursor]
        else:
            sectors = list(sectors)[cursor:]
        self._marks = [ t[0] for t in filter(selector, sectors) ]
        return self

    def finish(self):
        stroke = self._obj
        self._obj = copy(self._obj)
        return stroke

    def fwd(self, count=1):
        if count == -1:
            count = self._obj._baseLayer.getNSectors()
        direction = self._getDirection()
        sector = self._getPreviousForRelativeMovemnt()
        sectors = [ sector + (direction * i) for i in range(1, count+1) ]
        sectors = self._obj._modulo(sectors)
        self.add(*sectors)
        return self

    def rwd(self, count=1):
        if count == -1:
            count = self._obj._baseLayer.getNSectors()
        direction = self._getDirection()
        sector = self._getPreviousForRelativeMovemnt()
        sectors = [ sector - (direction * i) for i in range(1, count+1) ]
        sectors = self._obj._modulo(sectors)
        self.add(*sectors)
        return self

    def cw(self, count=1):
        if count == -1:
            count = self._obj._baseLayer.getNSectors()
        sector = self._getPreviousForRelativeMovemnt()
        sectors = [ sector + i for i in range(1, count+1) ]
        sectors = self._obj._modulo(sectors)
        self.add(*sectors)
        return self

    def ccw(self, count=1):
        if count == -1:
            count = self._obj._baseLayer.getNSectors()
        sector = self._getPreviousForRelativeMovemnt()
        sectors = [ sector - i for i in range(1, count+1) ]
        sectors = self._obj._modulo(sectors)
        self.add(*sectors)
        return self

    def add(self, *sectors):
        sectors = self._obj._decode(*sectors)
        cursor = self._cursor
        stroke = self._obj._stroke
        if self._editMode == "append":
            cursor = self._calcCursor(cursor, relative=1)
        stroke = stroke[:cursor] + sectors + stroke[cursor:]
        self._obj._stroke = stroke
        if self._cursor >= 0:
            self._cursor += len(sectors)
        self._updateMarksForInsertion(cursor, cursor + len(sectors))
        return self

    def addMovements(self, *movementsOrSectors):
        allowedMethods = [
            self.cw.__name__,
            self.ccw.__name__,
            self.fwd.__name__,
            self.rwd.__name__,
        ]
        for movementOrSector in movementsOrSectors:
            if movementOrSector in allowedMethods:
                getattr(self, movementOrSector)()
            else:
                self.add(movementOrSector)
        return self

    def delete(self, absolute=None, relative=0, mark=None):
        cursor = self._cursor
        stroke = self._obj._stroke

        # Delete from after the next position.
        if self._editMode == "append":
            if cursor == -1 or cursor == len(stroke):
                return self
            cursor = self._calcCursor(cursor, relative=1)

        # Default to delete from cursor to end.
        if absolute == None and not relative and mark == None:
            del stroke[cursor:]
            return self

        at = self._calcCursor(cursor, absolute=absolute, relative=relative, mark=mark)
        cursor, at = sorted([cursor, at])
        del stroke[cursor:at]
        self._updateMarksForDeletion(cursor, at)
        return self

    def _calcCursor(self, cursor, absolute=None, relative=0, mark=None):
        if mark != None:
            absolute = self._marks[mark]
        if absolute == None:
            if (cursor < 0 and cursor + relative >= 0):
                if cursor + relative == 0:
                    cursor = len(self._obj._stroke)
                else:
                    raise ValueError(f"Cursor moved beyond end: {self}")
            elif (cursor >= 0 and cursor + relative < 0):
                raise ValueError(f"Cursor moved beyond start: {self}")
            else:
                cursor += relative
        else:
            cursor = absolute + relative
        return cursor

    def _updateMarksForInsertion(self, a, b):
        a, b = sorted([a, b])
        diff = b - a

        def update(mark):
            if mark >= a:
                return mark + diff
            return mark

        self._marks = [ update(m) for m in self._marks ]

    def _updateMarksForDeletion(self, a, b):
        a, b = sorted([a, b])
        diff = b - a

        def update(mark):
            if mark >= a:
                return mark - diff
            return mark

        self._marks = [ update(m) for m in self._marks if not a <= m < b]

    def _previous(self, absolute=None, relative=0):
        if self._editMode != "append":
            relative -= 1
        at = self._calcCursor(self._cursor, absolute=absolute, relative=relative)
        return self._obj._stroke[at]

    def _getPreviousForRelativeMovemnt(self):
        sector = self._previous()
        if sector in self._obj._boundaries:
            raise ValueError(f"Unable to calculate relative movement:  previous sector '{self._obj._encode(sector)[0]}'")
        return sector

    def _getDirection(self, absolute=None, relative=0):
        if self._direction != None:
            return self._direction

        previous = self._previous(absolute=absolute, relative=relative)
        if previous in self._obj._boundaries:
            raise ValueError(f"Failed to get direction: {self}")
        beforePrevious = self._previous(absolute=absolute, relative=relative-1)
        if beforePrevious in self._obj._boundaries:
            raise ValueError(f"Failed to get direction: {self}")

        if previous - beforePrevious in (1, -self._obj._baseLayer.getNSectors() + 1):
            # Clockwise.
            self._direction = +1
        elif previous - beforePrevious in (-1, self._obj._baseLayer.getNSectors() - 1):
            # Counter-clockwise.
            self._direction = -1
        else:
            raise ValueError(f"Failed to get direction: {self}")

        return self._direction


class Mappings:
    def __init__(self):
        self._byStroke = {}
        self._byInput = {}
        self._byShiftedInput = {}
        self._bySection = {}

    def __add__(self, rhs):
        self._checkStrokeCollision(rhs)
        new = Mappings()
        for stroke, inputs in chain(self, rhs):
            new._add(*inputs, stroke)
        return new

    def __iadd__(self, rhs):
        self._checkStrokeCollision(rhs)
        for stroke, inputs in rhs:
            self._add(*inputs, stroke)
        return self

    def add(self, forInput, forShiftedInput, stroke):
        stroke.check()

        stroke = copy(stroke)
        self._checkSingleStroke((forInput, forShiftedInput), stroke)
        self._add(forInput, forShiftedInput, stroke)

    def allInputs(self):
        return self._byInput.keys()

    def allShiftedInputs(self):
        return self._byShiftedInput.keys()

    def check(self):
        self._checkStrokeCollision(rhs=self)

    def _checkStrokeCollision(self, rhs):
        for stroke, inputs in rhs._byStroke.items():
            self._checkSingleStroke(inputs, stroke)

    def _checkSingleStroke(self, inputs, stroke):
        # Only warn about duplicated mappings and keep going.
        # This check has to be done before actually inserting the given stroke.
        existingInputs = self._byStroke.get(stroke, None)
        if existingInputs == inputs:
            log(f"Ignored already existing stroke for {existingInputs}: {stroke}")
            return

        for section in stroke:
            forExistingStroke = self._byStroke.get(section, None)
            if forExistingStroke:
                startingMsg = "s starting" if section != stroke else ""
                raise RuntimeError(f"Stroke for {inputs} is ambiguous, it{startingMsg} conflicts with {forExistingStroke} in layer '{stroke.getLayerName()}': {section}")

        forExistingSections = self._bySection.get(stroke, None)
        if forExistingSections:
            raise RuntimeError(f"Stroke for {inputs} is ambiguous, its starting conflicts with {forExistingSections} in layer '{stroke.getLayerName()}': {section}")

    def _add(self, forInput, forShiftedInput, stroke):
        self._byStroke[stroke] = (forInput, forShiftedInput)
        self._byInput.setdefault(forInput, []).append(stroke)
        self._byShiftedInput.setdefault(forShiftedInput, []).append(stroke)
        for section in stroke:
            self._bySection.setdefault(section, []).append((forInput, forShiftedInput))

    def __getitem__(self, key):
        if type(key) == str:
            return self._byInput[key]
        elif isinstance(key, Stroke):
            return self._byStroke[key]
        else:
            raise TypeError("Invalid type of key")

    def __delitem__(self, key):
        if type(key) == str:
            strokes = self._byInput[key]
            if len(strokes) > 1:
                raise KeyError(f"Multiple strokes for '{key}', you have to choose the stroke to delete explicitly")
            key = strokes[0]
        if isinstance(key, Stroke):
            input, shiftedInput = self._byStroke.pop(key)
            self._byInput[input].remove(key)
            self._byShiftedInput[shiftedInput].remove(key)
            for section in key:
                self._bySection[section].remove((input, shiftedInput))
        else:
            raise TypeError("Invalid type of key")

    def __iter__(self):
        return iter(self._byStroke.items())

    def __str__(self):
        return "\n".join(
            f"{stroke.getLayerName()}: {inputs} == {stroke}" for stroke, inputs in self._byStroke.items()
        )


class LayerEditorBase:
    def __init__(self, layer: Layer, mappings: Mappings):
        self.layer = layer
        self.mappings = mappings


class LayoutSpecification:
    def __init__(self):
        self._types = {}

    def provideType(self, Type):
        self._types[Type.__name__] = Type

    def readFile(self, path):
        layer = None
        with open(path, "r") as layout:
            lines = self._massageInput(layout)
            for line in lines:
                typename, args = self._splitInputLine(line)
                Type, method = self._getType(typename)
                if issubclass(Type, Layer):
                    lowerChars = next(lines)
                    upperChars = next(lines)
                    layer = Type(lowerChars, upperChars, *args)
                elif issubclass(Type, LayerEditorBase):
                    if not layer:
                        raise InvalidFormat(f"A layer have to specified in advance: {typename}")
                    layer.extend(method, *args)
        return layer

    def _getType(self, typename):
        typenames = typename.split(".")
        context = globals() | self._types
        Type = context[typenames[0]]
        if len(typenames) == 1:
            return (Type, None)
        elif len(typenames) == 2:
            return (Type, getattr(Type, typenames[1]))
        raise InvalidFormat(f"Invalid typename: {typename}")

    @staticmethod
    def _massageInput(iterable):
        return ( e[:-1] for e in iterable if e != "\n" and not e.startswith("#") )

    @staticmethod
    def _splitInputLine(line):
        tokens = line.split(maxsplit=1)
        typename = tokens[0]
        if len(tokens) == 2:
            args = tokens[1].split()
        else:
            args = tuple()
        if typename[-1] != ":":
            raise InvalidFormat(f"Typename does not end with a colon: '{typename}'")
        return (typename[:-1], args)


class XmlLayout:
    _header="""\
<keyboardData>
    <keyboardCharacterSet>
        <lowerCase>{}</lowerCase>
        <upperCase>{}</upperCase>
    </keyboardCharacterSet>
    <keyboardActionMap>
"""
    _keyboardActionForString="""\
        <keyboardAction>
            <keyboardActionType>INPUT_TEXT</keyboardActionType>
            <movementSequence>{};</movementSequence>
            <inputString>{}</inputString>
            <inputCapsLockString>{}</inputCapsLockString>
        </keyboardAction>
"""
    _keyboardActionForKey="""\
        <keyboardAction>
            <keyboardActionType>INPUT_KEY</keyboardActionType>
            <movementSequence>{};</movementSequence>
            <inputKey>{}</inputKey>
        </keyboardAction>
"""
    _tail="""\
    </keyboardActionMap>
</keyboardData>
"""

    def __init__(self, path, characterSet):
        self._path = path
        self._characterSet = characterSet
        self._layoutFile = None

    def __enter__(self):
        self._layoutFile = open(self._path, "w")
        characterSet = [ self._quote(e) for e in self._characterSet ]
        self._layoutFile.write(
            self._header.format(*characterSet)
        )
        return self

    def __exit__(self, exType, exValue, traceback):
        self._layoutFile.write(self._tail)
        self._layoutFile.close()

    def addMappings(self, mappings):
        for stroke, (input, shiftedInput) in mappings:
            if input == shiftedInput and self.isKeycode(input):
                self.addKeyboardAction(str(stroke), inputKey=input)
            else:
                self.addKeyboardAction(str(stroke), inputString=input, inputCapslockString=shiftedInput)

    def addKeyboardAction(self, movementSequence, inputString=None, inputCapslockString=None, inputKey=None):
        if inputKey != None:
            keyboardAction = self._keyboardActionForKey.format(movementSequence, inputKey)
        else:
            inputString = self._quote(inputString)
            inputCapslockString = self._quote(inputCapslockString)
            keyboardAction = self._keyboardActionForString.format(movementSequence, inputString, inputCapslockString)
        self._layoutFile.write(keyboardAction)

    @staticmethod
    def isKeycode(string):
        return string == "SHIFT_TOGGLE" or string.startswith("KEYCODE_")

    @staticmethod
    def _quote(string):
        #TODO: There is potentially more to quote.
        def translate(item):
            if item == ">":
                return "&gt;"
            if item == "<":
                return "&lt;"
            return item
        return "".join([ translate(e) for e in string ])


def log(*args, file=stderr, **kwargs):
    print(*args, **kwargs, file=file)

def pairwise(iterable):
    rem = iterable
    while True:
        yield (rem[0], rem[1])
        rem = rem[2:]
        if len(rem) < 2:
            return

def forEachPrefix(iterable, *separators):
    for i, element in enumerate(iterable):
        if i == 0:
            continue
        if element in separators or i == len(iterable) - 1:
            yield iterable[:i+1]

def selectNOutOfSequence(sequence, count):
    n = len(sequence) // 2
    if count == 1:
        return [sequence[n // 2]]
    c1 = count // 2
    c2 = count - c1
    s1 = selectNOutOfSequence(sequence[:n], c1)
    s2 = selectNOutOfSequence(sequence[n:], c2)
    return s1 + s2


# vim: set ft=python:
